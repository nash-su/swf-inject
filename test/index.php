<?php

require dirname(__DIR__) . '/vendor/autoload.php';

use Swf\Inject\Injector as Injector;

#[\Swf\Inject\Attributes\Share]
#[\Swf\Inject\Attributes\Prepare('onMake')]
#[\Swf\Inject\Attributes\Define([':age' => 12])]
class DemoClass {
    public function __construct(private int $age)
    {
    }

    public static function onMake(self $a)
    {
        var_dump($a);
    }
}

Injector::initClass(DemoClass::class);

var_dump(Injector::instance()->make(DemoClass::class) instanceof DemoClass);

var_dump(Injector::instance()->make(DemoClass::class) === Injector::instance()->make(DemoClass::class));
