<?php

namespace Swf\Inject\Attributes;

use Attribute;

abstract class Inject
{
    abstract public function __invoke(string $className);
}
