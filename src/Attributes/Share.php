<?php

namespace Swf\Inject\Attributes;

use Attribute;
use Swf\Inject\Injector;
use Swf\Inject\Throwable\{InjectionException, ConfigException};

#[Attribute(Attribute::TARGET_CLASS)]
class Share extends Inject
{
    /**
     * @param string $className
     * @return void
     * @throws ConfigException
     * @throws InjectionException
     */
    public function __invoke(string $className)
    {
        Injector::instance()->share($className);
    }
}
