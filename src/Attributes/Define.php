<?php

namespace Swf\Inject\Attributes;

use Attribute;
use Swf\Inject\Injector;
use Swf\Inject\Throwable\InjectionException;

#[Attribute(Attribute::TARGET_CLASS)]
class Define extends Inject
{
    /**
     * @param array $defineArray
     */
    public function __construct(private array $defineArray)
    {}

    /**
     * @param string $className
     * @return void
     * @throws InjectionException
     */
    public function __invoke(string $className)
    {
        Injector::instance()->define($className, $this->defineArray);
    }
}
