<?php

namespace Swf\Inject\Attributes;

use Attribute;
use Swf\Inject\Injector;
use Swf\Inject\Throwable\InjectionException;

#[Attribute(Attribute::TARGET_CLASS)]
class Prepare extends Inject
{
    /**
     * @param string $method
     */
    public function __construct(private string $method)
    {}

    /**
     * @param string $className
     * @return void
     * @throws InjectionException
     */
    public function __invoke(string $className)
    {
        Injector::instance()->prepare($className, [$className, $this->method]);
    }
}
