<?php

namespace Swf\Inject\Attributes;

use Attribute;
use Swf\Inject\Injector;
use Swf\Inject\Throwable\{ConfigException, InjectionException};

#[Attribute(Attribute::TARGET_CLASS)]
class Delegate extends Inject
{
    /**
     * @param string $method
     */
    public function __construct(private string $method = 'instance')
    {}

    /**
     * @param string $className
     * @return void
     * @throws ConfigException
     * @throws InjectionException
     */
    public function __invoke(string $className)
    {
        Injector::instance()->delegate($className, [$className, $this->method]);
    }
}
