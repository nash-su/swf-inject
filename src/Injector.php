<?php

namespace Swf\Inject;

use Auryn\{Injector as RealInjector, ConfigException, InjectionException};
use ReflectionClass, ReflectionException;
use Swf\Inject\Attributes\Inject;

final class Injector
{
    /**
     * @var RealInjector|null
     */
    protected static ?RealInjector $realInjector = null;

    /**
     * @return RealInjector
     */
    public static function getRealInjector(): RealInjector
    {
        empty(self::$realInjector) && self::$realInjector = new RealInjector;
        return self::$realInjector;
    }

    /**
     * @throws Throwable\ConfigException
     */
    private function __construct()
    {
        $this->share($this);
    }

    /**
     * @param string $name
     * @param array $args
     * @return mixed
     * @throws Throwable\InjectionException
     */
    public function make(string $name, array $args = array()): mixed
    {
        try {
            return self::getRealInjector()->make($name, $args);
        } catch (InjectionException $e) {
            throw new Throwable\InjectionException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string|object $nameOrInstance
     * @return $this
     * @throws Throwable\ConfigException
     */
    public function share(string|object $nameOrInstance): self
    {
        try {
            self::getRealInjector()->share($nameOrInstance);
            return $this;
        } catch (ConfigException $e) {
            throw new Throwable\ConfigException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string $name
     * @param array $args
     * @return $this
     */
    public function define(string $name, array $args): self
    {
        self::getRealInjector()->define($name, $args);
        return $this;
    }

    /**
     * @param string $original
     * @param string $alias
     * @return $this
     * @throws Throwable\ConfigException
     */
    public function alias(string $original, string $alias): self
    {
        try {
            self::getRealInjector()->alias($original, $alias);
            return $this;
        } catch (ConfigException $e) {
            throw new Throwable\ConfigException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string $name
     * @param callable $callableOrMethodStr
     * @return $this
     * @throws Throwable\ConfigException
     */
    public function delegate(string $name, callable $callableOrMethodStr): self
    {
        try {
            self::getRealInjector()->delegate($name, $callableOrMethodStr);
            return $this;
        } catch (ConfigException $e) {
            throw new Throwable\ConfigException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string $name
     * @param callable $callableOrMethodStr
     * @return $this
     * @throws Throwable\InjectionException
     */
    public function prepare(string $name, callable $callableOrMethodStr): self
    {
        try {
            self::getRealInjector()->prepare($name, $callableOrMethodStr);
            return $this;
        } catch (InjectionException $e) {
            throw new Throwable\InjectionException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param callable $callableOrMethodStr
     * @param array $args
     * @return mixed
     * @throws Throwable\InjectionException
     */
    public function execute(callable $callableOrMethodStr, array $args = array()): mixed
    {
        try {
            return self::getRealInjector()->execute($callableOrMethodStr, $args);
        } catch (InjectionException $e) {
            throw new Throwable\InjectionException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @return static
     * @throws Throwable\InjectionException
     */
    public static function instance(): self
    {
        try {
            return self::$realInjector ? self::getRealInjector()->make(self::class) : new self;
        } catch (InjectionException $e) {
            throw new Throwable\InjectionException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string $className
     * @return void
     * @throws ReflectionException
     */
    public static function initClass(string $className)
    {
        $reflectionClass = new ReflectionClass($className);
        $attributes = $reflectionClass->getAttributes();
        foreach ($attributes as $attribute) {
            if (!class_exists($attribute->getName()))
                continue;
            $attributeObj = $attribute->newInstance();
            if (!is_subclass_of($attributeObj, Inject::class))
                continue;
            call_user_func($attributeObj, $className);
        }
    }
}
