# swf-console

#### 介绍
命令行交互程序构建框架

#### 软件架构
本框架基于 rdlowrey/auryn 构建，配置内容使用注解实现，请参考test目录

#### 安装教程
compose require nash-swf/inject